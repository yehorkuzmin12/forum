﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    /// <summary>
    /// Repository manage the specific data in Database
    /// </summary>
    /// <typeparam name="TEntity">Entity to provide generic repository.</typeparam>
    public interface IRepo<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// Get all entities from Database
        /// </summary>
        /// <returns>Parametrized with enumeration of entity task</returns>
        Task<IEnumerable<TEntity>> GetAllAsync();

        /// <summary>
        /// Get entity from database by entity id
        /// </summary>
        /// <param name="id">Entity id to get entity</param>
        /// <returns>Parametrized with finding entity task</returns>
        Task<TEntity> GetByIdAsync(int id);

        /// <summary>
        /// Add entity to Database
        /// </summary>
        /// <param name="entity">Entity argument to add</param>
        /// <returns>Representation of asynchronous operation</returns>
        Task AddAsync(TEntity entity);

        /// <summary>
        /// Delete entity from Database
        /// </summary>
        /// <param name="entity">Entity argument to delete</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Delete entity from Database by entity id(primary key)
        /// </summary>
        /// <param name="id">Entity id of entity to delete</param>
        /// <returns>Representation of asynchronous operation</returns>
        Task DeleteByIdAsync(int id);

        /// <summary>
        /// Update certain entity in Database
        /// </summary>
        /// <param name="entity">New entity arguments</param>
        /// <returns>Representation of asynchronous operation</returns>
        Task Update(TEntity entity);
    }
}
