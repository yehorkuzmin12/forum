﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IResponseRepository : IRepo<Response>
    {
        /// <summary>
        /// Get all responses from Database joining all relationships
        /// </summary>
        /// <returns>Parametrized with enumeration of responses task</returns>
        Task<IEnumerable<Response>> GetAllWithDetailsAsync();

        /// <summary>
        /// Get response by response id from Database joining all relationships
        /// </summary>
        /// /// <param name="id">Response id of finding response</param>
        /// <returns>Parametrized with response task</returns>
        Task<Response> GetByIdWithDetailsAsync(int id);
    }
}
