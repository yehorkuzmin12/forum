﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ICommentRepository : IRepo<Comment>
    {
        /// <summary>
        /// Get all comments from Database joining all relationships
        /// </summary>
        /// <returns>Parametrized with enumeration of comments task</returns>
        Task<IEnumerable<Comment>> GetAllWithDetailsAsync();

        /// <summary>
        /// Get comment by comment id from Database joining all relationships
        /// </summary>
        /// <param name="id">Comment id of finding comment</param>
        /// <returns>Parametrized with comment task</returns>
        Task<Comment> GetByIdWithDetailsAsync(int id);
    }
}
