﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    /// <summary>
    /// Unit of Work let us to manage complicated system as whole object
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Property of Topic Repository
        /// </summary>
        ITopicRepository TopicRepository { get; }

        /// <summary>
        /// Property of Response Repository
        /// </summary>
        IResponseRepository ResponseRepository { get; }

        /// <summary>
        /// Property of Comment Repository
        /// </summary>
        ICommentRepository CommentRepository { get; }

        /// <summary>
        /// Property of User Repository
        /// </summary>
        IUserRepository UserRepository { get; }

        /// <summary>
        /// Property of Tag Repository
        /// </summary>
        ITagRepository TagRepository { get; }

        /// <summary>
        /// Property of Topic Tag Repository
        /// </summary>
        ITopicTagRepository TopicTagRepository { get; }

        /// <summary>
        /// Property of Liker Response Repository
        /// </summary>
        ILikerResponseRepository LikerResponseRepository { get; }

        /// <summary>
        /// Property of Liker Topic Repository
        /// </summary>
        ILikerTopicRepository LikerTopicRepository { get; }

        /// <summary>
        /// Save all changes in repositories to Database
        /// </summary>
        /// <returns>Representation of asynchronous operation</returns>
        Task SaveAsync();
    }
}
