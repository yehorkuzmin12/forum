﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IUserRepository : IRepo<User>
    {
        /// <summary>
        /// Get all users from Database joining all relationships
        /// </summary>
        /// <returns>Parametrized with enumeration of users task</returns>
        Task<IEnumerable<User>> GetAllWithDetailsAsync();

        /// <summary>
        /// Get user by user id from Database joining all relationships
        /// </summary>
        /// <param name="id">User id of finding user</param>
        /// <returns>Parametrized with user task</returns>
        Task<User> GetByIdWithDetailsAsync(int id);
    }
}
