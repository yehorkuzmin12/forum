﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ITopicTagRepository
    {
        /// <summary>
        /// Add many to many model to Database
        /// </summary>
        /// <param name="entity">Entity argument to add</param>
        /// <returns>Representation of asynchronous operation</returns>
        Task AddAsync(TopicTag entity);

        /// <summary>
        /// Delete many to many entity from Database
        /// </summary>
        /// <param name="entity">Entity argument to delete</param>
        void Delete(TopicTag entity);
    }
}
