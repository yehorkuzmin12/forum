﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ITopicRepository : IRepo<Topic>
    {
        /// <summary>
        /// Get all topics from Database joining all relationships
        /// </summary>
        /// <returns>Parametrized with enumeration of topics task</returns>
        Task<IEnumerable<Topic>> GetAllWithDetailsAsync();

        /// <summary>
        /// Get topic by topic id from Database joining all relationships
        /// </summary>
        /// <param name="id">Topic id of finding topic</param>
        /// <returns>Parametrized with topic task</returns>
        Task<Topic> GetByIdWithDetailsAsync(int id);
    }
}
