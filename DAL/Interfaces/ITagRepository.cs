﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
   public interface ITagRepository : IRepo<Tag>
   {
        /// <summary>
        /// Get all tags from Database joining all relationships
        /// </summary>
        /// <returns>Parametrized with enumeration of tags task</returns>
        Task<IEnumerable<Tag>> GetAllWithDetailsAsync();

        /// <summary>
        /// Get tag by tag id from Database joining all relationships
        /// </summary>
        /// <param name="id">Tag id of finding tag</param>
        /// <returns>Parametrized with tag task</returns>
        Task<Tag> GetByIdWithDetailsAsync(int id);
    }
}
