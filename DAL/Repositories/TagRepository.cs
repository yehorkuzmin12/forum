﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Data;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    /// <inheritdoc />
    public class TagRepository : ITagRepository
    {
        private ForumDataContext _dbContext;

        /// <summary>
        /// Repository Constructor
        /// </summary>
        /// <param name="context">Forum Data contextx</param>
        public TagRepository(ForumDataContext context)
        {
            _dbContext = context;
        }

        /// <inheritdoc />
        public async Task AddAsync(Tag entity)
        {
            await _dbContext.Tags.AddAsync(entity);
        }

        /// <inheritdoc />
        public void Delete(Tag entity)
        {
            _dbContext.Tags.Remove(entity);
        }

        /// <inheritdoc />
        public async Task DeleteByIdAsync(int id)
        {
            await Task.Run(() => _dbContext.Tags.Remove(_dbContext.Tags.Find(id)));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Tag>> GetAllAsync()
        {
            return await _dbContext.Tags.ToListAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Tag>> GetAllWithDetailsAsync()
        {
            return await _dbContext.Tags
                .Include(t => t.TopicTags)
                .ThenInclude(tt => tt.Topic)
                .ToListAsync();
        }

        /// <inheritdoc />
        public async Task<Tag> GetByIdAsync(int id)
        {
            return await _dbContext.Tags.FindAsync(id);
        }

        /// <inheritdoc />
        public async Task<Tag> GetByIdWithDetailsAsync(int id)
        {
            return await _dbContext.Tags
               .Include(t => t.TopicTags)
               .ThenInclude(tt => tt.Topic)
               .FirstAsync(t => t.Id == id);
        }

        /// <inheritdoc />
        public async Task Update(Tag entity)
        {
            await DeleteByIdAsync(entity.Id);
            _dbContext.Tags.Update(entity);
        }
    }
}
