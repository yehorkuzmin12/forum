﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Data;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    /// <inheritdoc />
    public class LikerTopicRepository : ILikerTopicRepository
    {
        private ForumDataContext _dbContext;

        /// <summary>
        /// Repository Constructor
        /// </summary>
        /// <param name="context">Forum Data contextx</param>
        public LikerTopicRepository(ForumDataContext context)
        {
            _dbContext = context;
        }

        /// <inheritdoc />
        public async Task AddAsync(LikerTopic entity)
        {
            await _dbContext.LikerTopics.AddAsync(entity);
        }

        /// <inheritdoc />
        public void Delete(LikerTopic entity)
        {
            _dbContext.LikerTopics.Remove(entity);
        }
    }
}
