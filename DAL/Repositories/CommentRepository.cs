﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Data;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    /// <inheritdoc />
    public class CommentRepository : ICommentRepository
    {
        private ForumDataContext _dbContext;

        /// <summary>
        /// Repository Constructor
        /// </summary>
        /// <param name="context">Forum Data contextx</param>
        public CommentRepository(ForumDataContext context)
        {
            _dbContext = context;
        }

        /// <inheritdoc />
        public async Task AddAsync(Comment entity)
        {
            await _dbContext.Comments.AddAsync(entity);
        }

        /// <inheritdoc />
        public void Delete(Comment entity)
        {
            _dbContext.Comments.Remove(entity);
        }

        /// <inheritdoc />
        public async Task DeleteByIdAsync(int id)
        {
            await Task.Run(() => _dbContext.Comments.Remove(_dbContext.Comments.Find(id)));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Comment>> GetAllAsync()
        {
            return await _dbContext.Comments.ToListAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Comment>> GetAllWithDetailsAsync()
        {
            return await _dbContext.Comments
                .Include(c => c.Author)
                .Include(c => c.Response)
                .ThenInclude(r => r.Topic)
                .ToListAsync();
        }

        /// <inheritdoc />
        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _dbContext.Comments.FindAsync(id);
        }

        /// <inheritdoc />
        public async Task<Comment> GetByIdWithDetailsAsync(int id)
        {
            return await _dbContext.Comments
               .Include(c => c.Author)
               .Include(c => c.Response)
               .ThenInclude(r => r.Topic)
               .FirstAsync(c => c.Id == id);
               
        }

        /// <inheritdoc />
        public async Task Update(Comment entity)
        {
            await DeleteByIdAsync(entity.Id);
            _dbContext.Comments.Update(entity);
        }
    }
}
