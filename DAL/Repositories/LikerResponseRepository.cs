﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Data;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    /// <inheritdoc />
    public class LikerResponseRepository : ILikerResponseRepository
    {

        private ForumDataContext _dbContext;

        /// <summary>
        /// Repository Constructor
        /// </summary>
        /// <param name="context">Forum Data contextx</param>
        public LikerResponseRepository(ForumDataContext context)
        {
            _dbContext = context;
        }

        /// <inheritdoc />
        public async Task AddAsync(LikerResponse entity)
        {
            await _dbContext.LikerResponses.AddAsync(entity);
        }

        /// <inheritdoc />
        public void Delete(LikerResponse entity)
        {
            _dbContext.LikerResponses.Remove(entity);
        }
    }
}
