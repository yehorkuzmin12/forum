﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Data;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    /// <inheritdoc />
    public class TopicTagRepository : ITopicTagRepository
    {
        private ForumDataContext _dbContext;

        /// <summary>
        /// Repository Constructor
        /// </summary>
        /// <param name="context">Forum Data contextx</param>
        public TopicTagRepository(ForumDataContext context)
        {
            _dbContext = context;
        }

        /// <inheritdoc />
        public async Task AddAsync(TopicTag entity)
        {
            await _dbContext.TopicTags.AddAsync(entity);
        }

        /// <inheritdoc />
        public void Delete(TopicTag entity)
        {
            _dbContext.TopicTags.Remove(entity);
        }
    }
}
