﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Data;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    /// <inheritdoc />
    public class UserRepository : IUserRepository
    {
        private ForumDataContext _dbContext;

        /// <summary>
        /// Repository Constructor
        /// </summary>
        /// <param name="context">Forum Data contextx</param>
        public UserRepository(ForumDataContext context)
        {
            _dbContext = context;
        }

        /// <inheritdoc />
        public async Task AddAsync(User entity)
        {
            await _dbContext.RegisteredUsers.AddAsync(entity);
        }

        /// <inheritdoc />
        public void Delete(User entity)
        {
            _dbContext.RegisteredUsers.Remove(entity);
        }

        /// <inheritdoc />
        public async Task DeleteByIdAsync(int id)
        {
            await Task.Run(() => _dbContext.RegisteredUsers.Remove(_dbContext.RegisteredUsers.Find(id)));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _dbContext.RegisteredUsers.ToListAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<User>> GetAllWithDetailsAsync()
        {
            return await _dbContext.RegisteredUsers
                .Include(u => u.CreatedTopics)
                .Include(u => u.CreatedResponses)
                .Include(u => u.CreatedComments)
                .Include(u => u.LikedTopics)
                .ThenInclude(lt => lt.Topic)
                .Include(u => u.LikedResponses)
                .ThenInclude(lr => lr.Response)
                .Include(u => u.UserCredentials)
                .ToListAsync();
        }

        /// <inheritdoc />
        public async Task<User> GetByIdAsync(int id)
        {
            return await _dbContext.RegisteredUsers.FindAsync(id);
        }

        /// <inheritdoc />
        public async Task<User> GetByIdWithDetailsAsync(int id)
        {
            return await _dbContext.RegisteredUsers
               .Include(u => u.CreatedTopics)
               .Include(u => u.CreatedResponses)
               .Include(u => u.CreatedComments)
               .Include(u => u.LikedTopics)
               .ThenInclude(lt => lt.Topic)
               .Include(u => u.LikedResponses)
               .ThenInclude(lr => lr.Response)
               .Include(u => u.UserCredentials)
               .FirstAsync(u => u.Id == id);
        }

        /// <inheritdoc />
        public async Task Update(User entity)
        {
            await DeleteByIdAsync(entity.Id);
            _dbContext.RegisteredUsers.Update(entity);
        }
    }
}
