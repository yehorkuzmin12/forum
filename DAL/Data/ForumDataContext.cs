﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Configurations;
using DAL.Entities;
using DAL.Extensions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data
{
    /// <summary>
    /// Custom Data context inherited from IdentityDbContext to use data in Database
    /// </summary>
    public class ForumDataContext : IdentityDbContext<UserCredentials>
    {
        public virtual DbSet<Topic> Topics { get; set; }
        public virtual DbSet<Response> Responses { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<User> RegisteredUsers { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<TopicTag> TopicTags { get; set; }
        public virtual DbSet<LikerResponse> LikerResponses { get; set; }
        public virtual DbSet<LikerTopic> LikerTopics { get; set; }

        /// <summary>
        /// Forum Data Context Constructor
        /// </summary>
        /// <param name="options">Data context options</param>
        public ForumDataContext(DbContextOptions<ForumDataContext> options) : base(options)
        {
        }

        /// <summary>
        /// Default Forum Data Context Constructor
        /// </summary>
        public ForumDataContext() { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                .HasOne(u => u.UserCredentials);

            modelBuilder.Entity<Topic>()
                .HasOne(t => t.Author)
                .WithMany(u => u.CreatedTopics);

            modelBuilder.Entity<Topic>()
                .HasMany(t => t.Responses)
                .WithOne(r => r.Topic)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TopicTag>().Many2Many();
            modelBuilder.Entity<LikerTopic>().Many2Many();
            modelBuilder.Entity<LikerResponse>().Many2Many();

            modelBuilder.Entity<Response>()
                .HasOne(r => r.Author)
                .WithMany(u => u.CreatedResponses);

            modelBuilder.Entity<Response>()
                .HasMany(r => r.Comments)
                .WithOne(c => c.Response)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Comment>()
                .HasOne(c => c.Author)
                .WithMany(u => u.CreatedComments);

            modelBuilder.ApplyConfiguration(new RoleConfiguration());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ForumDb;Trusted_Connection=True;");
        }
    }
}
