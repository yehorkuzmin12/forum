﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class AddingIdentityRolesCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                table: "RegisteredUsers");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "7efeb974-c614-4430-be09-14b7f404fb14", "c7375b14-d0cd-4e4d-900a-fa40653d2fb3", "CommonUser", "COMMON_USER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "7950abc2-950d-4901-8cee-3a471b05b12d", "af44d65f-62e8-4d96-a955-7eeae0d521a5", "Moderator", "MODERATOR" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7950abc2-950d-4901-8cee-3a471b05b12d");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7efeb974-c614-4430-be09-14b7f404fb14");

            migrationBuilder.AddColumn<int>(
                name: "Role",
                table: "RegisteredUsers",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
