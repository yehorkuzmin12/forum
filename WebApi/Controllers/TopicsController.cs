﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    /// <inheritdoc />
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TopicsController : ControllerBase
    {
        private readonly ITopicService _service;
        public TopicsController(ITopicService service)
        {
            _service = service;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TopicModel>>> Get()
        {
            try
            {
                var topics = await _service.GetAllAsync();
                return Ok(topics);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("page/{pageNumber}")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TopicModel>>> GetByPageModel(int pageNumber)
        {
            try
            {
                PageModel pageModel = new PageModel { 
                    PageNumber = pageNumber,
                    PageSize = 8
                };

                var topics = await _service.GetByPageModelAsync(pageModel);
                return Ok(topics);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("tags")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TagModel>>> GetTags()
        {
            try
            {
                var tags = await _service.GetTagsAsync();
                return Ok(tags);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("filter")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TopicModel>>> GetByFilter([FromQuery] IEnumerable<int> tags, [FromQuery] DateTime? publicationDate)
        {
            try
            {
                FilterModel filter = new FilterModel
                {
                    TagIds = tags,
                    PublicationDate = publicationDate
                };

                var topics = await _service.GetByFilterAsync(filter);
                return Ok(topics);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<TopicModel>> GetById(int id)
        {
            try
            {
                TopicModel topic = await _service.GetByIdAsync(id);
                return Ok(topic);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("sort/likes")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TopicModel>>> GetSortedByLikes()
        {
            try
            {
                var topics = await _service.SortByLikesAsync();
                return Ok(topics);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("sort/date")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TopicModel>>> GetSortedByDate()
        {
            try
            {
                var topics = await _service.SortByPublicationDateAsync();
                return Ok(topics);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Roles ="CommonUser, Moderator")]
        public async Task<ActionResult> Add([FromBody] TopicModel value)
        {
            try
            {
                await _service.AddAsync(value);
                return CreatedAtAction(nameof(Add), new { id = value.Id }, value);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("tag")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> AddTag([FromBody] TopicTagModel tag)
        {
            try
            {
                await _service.AddTagAsync(tag);
                return CreatedAtAction(nameof(AddTag), new { CompositeKey = Tuple.Create(tag.TagId, tag.TopicId) }, tag);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> Update(int Id, [FromBody] TopicModel value)
        {
            try
            {
                var findingTopic = await _service.GetByIdAsync(Id);

                if (findingTopic is null)
                    return NotFound();

                await _service.UpdateAsync(value);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}/complain")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> Complain(int id)
        {
            try
            {
                await _service.ComplainAboutTopicAsync(id);
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Moderator")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("tag")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Moderator")]
        public async Task<ActionResult> RemoveTag([FromBody] TopicTagModel tag)
        {
            try
            {
                await _service.RemoveTagAsync(tag);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
