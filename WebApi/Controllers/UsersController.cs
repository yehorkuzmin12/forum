﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace WebApi.Controllers
{
    /// <inheritdoc />
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly UserManager<UserCredentials> _userManager;
        private readonly SignInManager<UserCredentials> _signInManager;
        public IConfiguration Configuration { get; }
        public UsersController(IUserService service, 
            UserManager<UserCredentials> userManager, 
            SignInManager<UserCredentials> signInManager,
            IConfiguration config)
        {
            _service = service;
            _userManager = userManager;
            _signInManager = signInManager;
            Configuration = config;
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Moderator")]
        public async Task<ActionResult<IEnumerable<UserModel>>> Get()
        {
            try
            {
                var users = await _service.GetAllAsync();
                return Ok(users);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<UserModel>> GetById(int id)
        {
            try
            {
                var user = await _service.GetByIdAsync(id);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("topic/{topicId}")]
        [AllowAnonymous]
        public async Task<ActionResult<UserModel>> GetUserByTopicId(int topicId)
        {
            try
            {
                var user = await _service.GetByTopicIdAsync(topicId);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("response/{responseId}")]
        [AllowAnonymous]
        public async Task<ActionResult<UserModel>> GetUserByResponseId(int responseId)
        {
            try
            {
                var user = await _service.GetByResponseIdAsync(responseId);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("comment/{commentId}")]
        [AllowAnonymous]
        public async Task<ActionResult<UserModel>> GetUserByCommentId(int commentId)
        {
            try
            {
                var user = await _service.GetByCommentIdAsync(commentId);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}/topics")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult<IEnumerable<TopicModel>>> GetTopicsByUserId(int id)
        {
            try
            {
                var topics = await _service.GetTopicsByUserIdAsync(id);
                return Ok(topics);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}/responses")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult<IEnumerable<ResponseModel>>> GetResponsesByUserId(int id)
        {
            try
            {
                var responses = await _service.GetResponsesByUserIdAsync(id);
                return Ok(responses);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}/comments")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult<IEnumerable<CommentModel>>> GetCommentsByUserId(int id)
        {
            try
            {
                var comments = await _service.GetCommentsByUserIdAsync(id);
                return Ok(comments);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost("response/like/add")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> LikeResponse([FromBody] LikerResponseModel model)
        {
            try
            {
                await _service.LikeResponseAsync(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("topic/like/add")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> LikeTopic([FromBody] LikerTopicModel model)
        {
            try
            {
                await _service.LikeTopicAsync(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("response/like/remove")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> RemoveLikeResponse([FromBody] LikerResponseModel model)
        {
            try
            {
                await _service.RemoveLikeResponseAsync(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("topic/like/remove")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> RemoveLikeTopic([FromBody] LikerTopicModel model)
        {
            try
            {
                await _service.RemoveLikeTopicAsync(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("token")]
        [AllowAnonymous]
        public async Task<ActionResult> GetToken([FromBody] AuthenticationModel model)
        {
            var credentials = await _service.GetCredentials();
            var user = credentials.FirstOrDefault(c => c.Email == model.Email);

            var roles = await _userManager.GetRolesAsync(user);

            if (user is null)
            {
                return Unauthorized();
            }

            var users = await _service.GetAllAsync();
            var userDetail = users.FirstOrDefault(u => u.UserCredentialsId == user.Id);

            var signInResult = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);

            if (signInResult.Succeeded)
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.SerialNumber, userDetail.Id),
                        new Claim(ClaimTypes.Email, model.Email),
                        new Claim(ClaimTypes.Name, userDetail.Nickname),
                        new Claim(ClaimTypes.Role, roles.First())
                    }),
                    Expires = DateTime.Now.AddHours(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])), 
                        SecurityAlgorithms.HmacSha256),
                    Audience = Configuration["Jwt:Audience"],
                    Issuer = Configuration["Jwt:Issuer"]
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);

                return Ok(new { User = JsonSerializer.Serialize(userDetail), Token = tokenString });
            }

            return BadRequest("Try again!");
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<ActionResult> Register([FromBody] UserModel model)
        {
            UserCredentials credentials = new UserCredentials
            {
                Email = model.Email,
                UserName = model.Nickname,
                EmailConfirmed = false
            };

            var result = await _userManager.CreateAsync(credentials, model.Password);

            if (result.Succeeded)
            {
                try
                {
                    model.UserCredentialsId = credentials.Id;
                    await _service.AddAsync(model);
                    await _userManager.AddToRoleAsync(credentials, "COMMON_USER");
                    return Ok(new { Result = "Success Registered" });
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            StringBuilder errorBuilder = new StringBuilder();
            foreach (var item in result.Errors)
            {
                errorBuilder.Append(item.Description + "\r\n");
            }

            return BadRequest(new { Result = $"Register Fail: {errorBuilder.ToString()}" });
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles ="Moderator")]
        public async Task<ActionResult> Update(int Id, [FromBody] UserModel value)
        {
            try
            {
                var findingUser = await _service.GetByIdAsync(Id);

                if (findingUser is null)
                    return NotFound();

                await _service.UpdateAsync(value);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Moderator")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    } 
}
