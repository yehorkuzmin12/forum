﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    /// <inheritdoc />
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ResponsesController : ControllerBase
    {
        private readonly IResponseService _service;
        public ResponsesController(IResponseService service)
        {
            _service = service;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<ResponseModel>>> Get()
        {
            try
            {
                var responses = await _service.GetAllAsync();
                return Ok(responses);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<ResponseModel>> GetById(int id)
        {
            try
            {
                var response = await _service.GetByIdAsync(id);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("topic/{topicId}")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<ResponseModel>>> GetByTopicId(int topicId)
        {
            try
            {
                var responses = await _service.GetByTopicIdAsync(topicId);
                return Ok(responses);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("topic/{topicId}/page/{pageNumber}")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<ResponseModel>>> GetByPageModel(int topicId, int pageNumber)
        {
            try
            {
                PageModel pageModel = new PageModel
                {
                    PageNumber = pageNumber,
                    PageSize = 8
                };

                var responses = await _service.GetByPageModelAsync(topicId, pageModel);
                return Ok(responses);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("sort/activity")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<ResponseModel>>> GetSortedByLikes()
        {
            try
            {
                var responses = await _service.SortByLikesAsync();
                return Ok(responses);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("sort/publicationDate")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<ResponseModel>>> GetSortedByDate()
        {
            try
            {
                var responses = await _service.SortByPublicationDateAsync();
                return Ok(responses);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> Add([FromBody] ResponseModel value)
        {
            try
            {
                await _service.AddAsync(value);
                return CreatedAtAction(nameof(Add), new { id = value.Id }, value);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> Update(int Id, [FromBody] ResponseModel value)
        {
            try
            {
                var findingResponse = await _service.GetByIdAsync(Id);

                if (findingResponse is null)
                    return NotFound();

                await _service.UpdateAsync(value);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}/complain")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> Complain(int id)
        {
            try
            {
                await _service.ComplainAboutResponseAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Moderator")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
