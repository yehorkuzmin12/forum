﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    /// <inheritdoc />
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService _service;
        public CommentsController(ICommentService service)
        {
            _service = service;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<CommentModel>>> Get()
        {
            try
            {
                var comments = await _service.GetAllAsync();
                return Ok(comments);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<CommentModel>> GetById(int id)
        {
            try
            {
                var comment = await _service.GetByIdAsync(id);
                return Ok(comment);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("response/{responseId}")]
        [AllowAnonymous]
        public async Task<ActionResult<CommentModel>> GetByResponseId(int responseId)
        {
            try
            {
                var comments = await _service.GetByResponseId(responseId);
                return Ok(comments);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> Add([FromBody] CommentModel value)
        {
            try
            {
                await _service.AddAsync(value);
                return CreatedAtAction(nameof(Add), new { id = value.Id }, value);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> Update(int Id, [FromBody] CommentModel value)
        {
            try
            {
                var findingComment = await _service.GetByIdAsync(Id);

                if (findingComment is null)
                    return NotFound();

                await _service.UpdateAsync(value);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}/complain")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "CommonUser, Moderator")]
        public async Task<ActionResult> Complain(int id)
        {
            try
            {
                await _service.ComplainAboutCommentAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Moderator")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
