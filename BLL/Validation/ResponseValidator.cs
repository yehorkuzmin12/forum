﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Models;
using FluentValidation;

namespace BLL.Validation
{
    /// <summary>
    /// Fluent Validation Class to validate response model
    /// </summary>
    public class ResponseValidator : AbstractValidator<ResponseModel>
    {
        /// <summary>
        /// Validator Constructor to set validation rules
        /// </summary>
        public ResponseValidator()
        {
            RuleFor(rm => rm.Text).NotEmpty();
            RuleFor(rm => rm.ResponseState).IsInEnum();
            RuleFor(rm => rm.PublicationDate).GreaterThan(new DateTime(2022, 01, 01));
        }
    }
}
