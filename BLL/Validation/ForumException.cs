﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Validation
{
    /// <inheritdoc />
    public class ForumException : Exception
    {
        /// <inheritdoc />
        public ForumException() : base()
        {

        }

        /// <inheritdoc />
        public ForumException(string msg) : base(msg)
        {

        }

        /// <inheritdoc />
        public ForumException(string msg, Exception ex) : base(msg, ex)
        {

        }
    }
}
