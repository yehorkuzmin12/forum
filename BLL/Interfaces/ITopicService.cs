﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;

namespace BLL.Interfaces
{
    /// <inheritdoc />
    public interface ITopicService : IService<TopicModel>
    {
        /// <summary>
        /// Get topics using certain filter
        /// </summary>
        /// <param name="filter">Model that contains parameters to change count of topic models</param>
        /// <returns>Parametrized with enumeration of topic models task</returns>
        Task<IEnumerable<TopicModel>> GetByFilterAsync(FilterModel filter);

        /// <summary>
        /// Get topics by certain page on client
        /// </summary>
        /// <param name="pageModel">Model that contains parameters to fetch only part of topic models</param>
        /// <returns>Parametrized with enumeration of topic models task</returns>
        Task<IEnumerable<TopicModel>> GetByPageModelAsync(PageModel pageModel);

        /// <summary>
        /// Fetch all tags from Data Access Layer
        /// </summary>
        /// <returns>Parametrized with enumeration of tag models task</returns>
        Task<IEnumerable<TagModel>> GetTagsAsync();

        /// <summary>
        /// Sort fetched topics from Data Access Layer topic models by their publication Date
        /// </summary>
        /// <returns>Parametrized with enumeration of topic models task</returns>
        Task<IEnumerable<TopicModel>> SortByPublicationDateAsync();

        /// <summary>
        /// Sort fetched topics from Data Access Layer topic models by their likes count
        /// </summary>
        /// <returns>Parametrized with enumeration of topic models task</returns>
        Task<IEnumerable<TopicModel>> SortByLikesAsync();

        /// <summary>
        /// Update topic model with new one complaint
        /// </summary>
        /// <returns>Representation of asynchronous operation</returns>
        Task ComplainAboutTopicAsync(int topicId);

        /// <summary>
        /// Push new tag to adding method of Data Access Layer
        /// </summary>
        /// <returns>Representation of asynchronous operation</returns>
        Task AddTagAsync(TopicTagModel model);

        /// <summary>
        /// Push tag to delete method of Data Access Layer
        /// </summary>
        /// <returns>Representation of asynchronous operation</returns>
        Task RemoveTagAsync(TopicTagModel model);
    }
}
