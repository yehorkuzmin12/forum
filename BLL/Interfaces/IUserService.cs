﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;
using DAL.Entities;

namespace BLL.Interfaces
{
    /// <inheritdoc />
    public interface IUserService : IService<UserModel>
    {
        /// <summary>
        /// Get all responses by certain user id
        /// </summary>
        /// <param name="userId">User id to get responses with it</param>
        /// <returns>Parametrized with enumeration of response models task</returns>
        Task<IEnumerable<ResponseModel>> GetResponsesByUserIdAsync(int userId);

        /// <summary>
        /// Get all topics by certain user id
        /// </summary>
        /// <param name="userId">User id to get topics with it</param>
        /// <returns>Parametrized with enumeration of topics models task</returns>
        Task<IEnumerable<TopicModel>> GetTopicsByUserIdAsync(int userId);

        /// <summary>
        /// Get all comments by certain user id
        /// </summary>
        /// <param name="userId">User id to get comments with it</param>
        /// <returns>Parametrized with enumeration of comments models task</returns>
        Task<IEnumerable<CommentModel>> GetCommentsByUserIdAsync(int userId);

        /// <summary>
        /// Get all identity credentials of users
        /// </summary>
        /// <returns>Parametrized with enumeration of user credentials task</returns>
        Task<IEnumerable<UserCredentials>> GetCredentials();

        /// <summary>
        /// Get user by topic id
        /// </summary>
        /// <param name="topicId">Topic Id to get certain user</param>
        /// <returns>Parametrized with user model task</returns>
        Task<UserModel> GetByTopicIdAsync(int topicId);

        /// <summary>
        /// Get user by response id
        /// </summary>
        /// <param name="responseId">Response Id to get certain user</param>
        /// <returns>Parametrized with user model task</returns>
        Task<UserModel> GetByResponseIdAsync(int responseId);

        /// <summary>
        /// Get user by comment id
        /// </summary>
        /// <param name="commentId">Comment Id to get certain user</param>
        /// <returns>Parametrized with user model task</returns>
        Task<UserModel> GetByCommentIdAsync(int commentId);

        /// <summary>
        /// Push many to many liker topic model to adding Data Access Layer method
        /// </summary>
        /// <param name="model">Many to many model to create relationship between user and liked topic and push to Data Access Layer</param>
        /// <returns>Representation of asynchronous operation</returns>
        Task LikeTopicAsync(LikerTopicModel model);

        /// <summary>
        /// Push many to many liker response model to adding Data Access Layer method
        /// </summary>
        /// <param name="model">Many to many model to create relationship between user and liked response and push to Data Access Layer</param>
        /// <returns>Representation of asynchronous operation</returns>
        Task LikeResponseAsync(LikerResponseModel model);

        /// <summary>
        /// Push many to many liker topic model to delete Data Access Layer method
        /// </summary>
        /// <param name="model">Many to many model to remove relationship between user and liked topic and push to Data Access Layer</param>
        /// <returns>Representation of asynchronous operation</returns>
        Task RemoveLikeTopicAsync(LikerTopicModel model);

        /// <summary>
        /// Push many to many liker response model to delete Data Access Layer method
        /// </summary>
        /// <param name="model">Many to many model to remove relationship between user and liked response and push to Data Access Layer</param>
        /// <returns>Representation of asynchronous operation</returns>
        Task RemoveLikeResponseAsync(LikerResponseModel model);
    }
}
