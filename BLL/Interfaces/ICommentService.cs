﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;

namespace BLL.Interfaces
{
    /// <inheritdoc />
    public interface ICommentService : IService<CommentModel>
    {
        /// <summary>
        /// Update comment model with new one complaint
        /// </summary>
        /// <returns>Representation of asynchronous operation</returns>
        Task ComplainAboutCommentAsync(int commentId);

        /// <summary>
        /// Get all comments with certain response id
        /// </summary>
        /// <param name="responseId">Response id to get comments with it</param>
        /// <returns>Parametrized with enumeration of comment models task</returns>
        Task<IEnumerable<CommentModel>> GetByResponseId(int responseId);
    }
}
