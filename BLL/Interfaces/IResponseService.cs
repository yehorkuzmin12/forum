﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;

namespace BLL.Interfaces
{
    /// <inheritdoc />
    public interface IResponseService : IService<ResponseModel>
    {
        /// <summary>
        /// Update response model with new one complaint
        /// </summary>
        /// <returns>Representation of asynchronous operation</returns>
        Task ComplainAboutResponseAsync(int responseId);

        /// <summary>
        /// Get responses by certain page on client
        /// </summary>
        /// <param name="pageModel">Model that contains parameters to fetch only part of response models</param>
        /// <returns>Parametrized with enumeration of response models task</returns>
        Task<IEnumerable<ResponseModel>> GetByPageModelAsync(int topicId, PageModel pageModel);

        /// <summary>
        /// Get all responses with certain topic id
        /// </summary>
        /// <param name="topicId">Topic id to get responses with it</param>
        /// <returns>Parametrized with enumeration of response models task</returns>
        Task<IEnumerable<ResponseModel>> GetByTopicIdAsync(int topicId);

        /// <summary>
        /// Sort fetched responses from Data Access Layer topic models by their publication Date
        /// </summary>
        /// <returns>Parametrized with enumeration of response models task</returns>
        Task<IEnumerable<ResponseModel>> SortByPublicationDateAsync();

        /// <summary>
        /// Sort fetched responses from Data Access Layer topic models by their likes count
        /// </summary>
        /// <returns>Parametrized with enumeration of response models task</returns>
        Task<IEnumerable<ResponseModel>> SortByLikesAsync();
    }
}
