﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;

namespace BLL.Interfaces
{

    /// <summary>
    /// Services provide logic over repository`s pulled data
    /// </summary>
    /// <typeparam name="TModel">Model to provide generic service interface</typeparam>
    public interface IService<TModel> where TModel : class
    {
        /// <summary>
        /// Get all entities from Data Access Layer to process them in Business Logic Layer
        /// </summary>
        /// <returns>Parametrized with enumeration of models task</returns>
        Task<IEnumerable<TModel>> GetAllAsync();

        /// <summary>
        /// Get entity by entity id from Data Access Layer to process it in Business Logic Layer
        /// </summary>
        /// <param name="id">Model id to map with entity</param>
        /// <returns>Parametrized with model task</returns>
        Task<TModel> GetByIdAsync(int id);

        /// <summary>
        /// Push entity to adding method in Data Access Layer
        /// </summary>
        /// <param name="model">Model to map with entity</param>
        /// <returns></returns>
        Task AddAsync(TModel model);

        /// <summary>
        /// Push entity to update method in Data Access Layer
        /// </summary>
        /// <param name="model">Model to map with entity</param>
        Task UpdateAsync(TModel model);

        /// <summary>
        /// Push entity to delete method in Data Access Layer
        /// </summary>
        /// <param name="modelId">Model id to map with entity</param>
        Task DeleteAsync(int modelId);
    }
}
