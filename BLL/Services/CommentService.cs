﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using FluentValidation;

namespace BLL.Services
{
    /// <inheritdoc />
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<CommentModel> _validator;

        /// <summary>
        /// Comment Service Constructor
        /// </summary>
        /// <param name="uow">Unit of work</param>
        /// <param name="mapper">Custom Mapper</param>
        /// <param name="validator">Custom Validator for Model</param>
        public CommentService(IUnitOfWork uow, IMapper mapper, AbstractValidator<CommentModel> validator)
        {
            _uow = uow;
            _mapper = mapper;
            _validator = validator;
        }

        /// <inheritdoc />
        public async Task AddAsync(CommentModel model)
        {
            if (_validator.Validate(model).Errors.Count != 0)
            {
                throw new ForumException("The comment has incorrect data!");
            }

            var comment = _mapper.Map<Comment>(model);

            await _uow.CommentRepository.AddAsync(comment);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task ComplainAboutCommentAsync(int commentId)
        {
            var comment = await _uow.CommentRepository.GetByIdWithDetailsAsync(commentId);
            comment.Complaints++;

            await _uow.CommentRepository.Update(comment);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task DeleteAsync(int modelId)
        {
            await _uow.CommentRepository.DeleteByIdAsync(modelId);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<CommentModel>> GetAllAsync()
        {
            var comments = await _uow.CommentRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<CommentModel>>(comments);
        }

        /// <inheritdoc />
        public async Task<CommentModel> GetByIdAsync(int id)
        {
            var comment = await _uow.CommentRepository.GetByIdWithDetailsAsync(id);
            return _mapper.Map<CommentModel>(comment);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<CommentModel>> GetByResponseId(int responseId)
        {
            var comments = await _uow.CommentRepository.GetAllWithDetailsAsync();
            comments = comments.Where(c => c.ResponseId == responseId);
            return _mapper.Map<IEnumerable<CommentModel>>(comments);
        }

        /// <inheritdoc />
        public async Task UpdateAsync(CommentModel model)
        {
            if (_validator.Validate(model).Errors.Count != 0)
            {
                throw new ForumException("The comment has incorrect data!");
            }

            var comment = _mapper.Map<Comment>(model);

            await _uow.CommentRepository.Update(comment);
            await _uow.SaveAsync();
        }
    }
}
