﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using FluentValidation;

namespace BLL.Services
{
    /// <inheritdoc />
    public class ResponseService : IResponseService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<ResponseModel> _validator;

        /// <summary>
        /// Response Service Constructor
        /// </summary>
        /// <param name="uow">Unit of work</param>
        /// <param name="mapper">Custom Mapper</param>
        /// <param name="validator">Custom Validator for Model</param>
        public ResponseService(IUnitOfWork uow, IMapper mapper, AbstractValidator<ResponseModel> validator)
        {
            _uow = uow;
            _mapper = mapper;
            _validator = validator;
        }

        /// <inheritdoc />
        public async Task AddAsync(ResponseModel model)
        {
            if (_validator.Validate(model).Errors.Count != 0)
            {
                throw new ForumException("The response has incorrect data!");
            }

            var response = _mapper.Map<Response>(model);

            await _uow.ResponseRepository.AddAsync(response);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task ComplainAboutResponseAsync(int responseId)
        {
            var response = await _uow.ResponseRepository.GetByIdWithDetailsAsync(responseId);
            response.Complaints++;

            await _uow.ResponseRepository.Update(response);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task DeleteAsync(int modelId)
        {
            await _uow.ResponseRepository.DeleteByIdAsync(modelId);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<ResponseModel>> GetAllAsync()
        {
            var responses = await _uow.ResponseRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<ResponseModel>>(responses);
        }

        /// <inheritdoc />
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = await _uow.ResponseRepository.GetByIdWithDetailsAsync(id);
            return _mapper.Map<ResponseModel>(response);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<ResponseModel>> GetByPageModelAsync(int topicId, PageModel pageModel)
        {
            var responses = await GetByTopicIdAsync(topicId);
            responses = responses.Skip((pageModel.PageNumber - 1) * pageModel.PageSize)
            .Take(pageModel.PageSize);
            return _mapper.Map<IEnumerable<ResponseModel>>(responses);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<ResponseModel>> GetByTopicIdAsync(int topicId)
        {
            var responses = await _uow.ResponseRepository.GetAllWithDetailsAsync();
            responses = responses.Where(r => r.TopicId == topicId);
            return _mapper.Map<IEnumerable<ResponseModel>>(responses);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<ResponseModel>> SortByLikesAsync()
        {
            var responses = await _uow.ResponseRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<ResponseModel>>(responses.OrderByDescending(r => r.LikedBy.Count));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<ResponseModel>> SortByPublicationDateAsync()
        {
            var responses = await _uow.ResponseRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<ResponseModel>>(responses.OrderByDescending(r => r.PublicationDate));
        }

        /// <inheritdoc />
        public async Task UpdateAsync(ResponseModel model)
        {
            if (_validator.Validate(model).Errors.Count != 0)
            {
                throw new ForumException("The response has incorrect data!");
            }

            var response = _mapper.Map<Response>(model);

            await _uow.ResponseRepository.Update(response);
            await _uow.SaveAsync();
        }
    }
}
