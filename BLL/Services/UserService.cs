﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using FluentValidation;

namespace BLL.Services
{
    /// <inheritdoc />
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<UserModel> _validator;

        /// <summary>
        /// User Service Constructor
        /// </summary>
        /// <param name="uow">Unit of work</param>
        /// <param name="mapper">Custom Mapper</param>
        /// <param name="validator">Custom Validator for Model</param>
        public UserService(IUnitOfWork uow, IMapper mapper, AbstractValidator<UserModel> validator)
        {
            _uow = uow;
            _mapper = mapper;
            _validator = validator;
        }

        /// <inheritdoc />
        public async Task AddAsync(UserModel model)
        {
            if(_validator.Validate(model).Errors.Count != 0)
            {
                throw new ForumException("The user has incorrect data!");
            }

            var credentials = await GetCredentials();

            var user = _mapper.Map<User>(model);
            user.UserCredentials = credentials.FirstOrDefault(c => c.PasswordHash != null && c.Email == model.Email);

            await _uow.UserRepository.AddAsync(user);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task DeleteAsync(int modelId)
        {
            await _uow.UserRepository.DeleteByIdAsync(modelId);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<UserModel>> GetAllAsync()
        {
            var users = await _uow.UserRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<UserModel>>(users);
        }

        /// <inheritdoc />
        public async Task<UserModel> GetByCommentIdAsync(int commentId)
        {
            var users = await _uow.UserRepository.GetAllWithDetailsAsync();
            var user = users.First(u => u.CreatedComments.Select(c => c.Id).Contains(commentId));
            return _mapper.Map<UserModel>(user);
        }

        /// <inheritdoc />
        public async Task<UserModel> GetByIdAsync(int id)
        {
            var user = await _uow.UserRepository.GetByIdWithDetailsAsync(id);
            return _mapper.Map<UserModel>(user);
        }

        /// <inheritdoc />
        public async Task<UserModel> GetByResponseIdAsync(int responseId)
        {
            var users = await _uow.UserRepository.GetAllWithDetailsAsync();
            var user = users.First(u => u.CreatedResponses.Select(c => c.Id).Contains(responseId));
            return _mapper.Map<UserModel>(user);
        }

        /// <inheritdoc />
        public async Task<UserModel> GetByTopicIdAsync(int topicId)
        {
            var users = await _uow.UserRepository.GetAllWithDetailsAsync();
            var user = users.First(u => u.CreatedTopics.Select(c => c.Id).Contains(topicId));
            return _mapper.Map<UserModel>(user);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<CommentModel>> GetCommentsByUserIdAsync(int userId)
        {
            var user = await _uow.UserRepository.GetByIdWithDetailsAsync(userId);
            return _mapper.Map<IEnumerable<CommentModel>>(user.CreatedComments.AsEnumerable());
        }

        /// <inheritdoc />
        public async Task<IEnumerable<UserCredentials>> GetCredentials()
        {
            var users = await _uow.UserRepository.GetAllWithDetailsAsync();
            var credentials = users.Select(u => u.UserCredentials).ToList();

            return credentials;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<ResponseModel>> GetResponsesByUserIdAsync(int userId)
        {
            var user = await _uow.UserRepository.GetByIdWithDetailsAsync(userId);
            return _mapper.Map<IEnumerable<ResponseModel>>(user.CreatedResponses.AsEnumerable());
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TopicModel>> GetTopicsByUserIdAsync(int userId)
        {
            var user = await _uow.UserRepository.GetByIdWithDetailsAsync(userId);
            return _mapper.Map<IEnumerable<TopicModel>>(user.CreatedTopics.AsEnumerable());
        }

        /// <inheritdoc />
        public async Task LikeResponseAsync(LikerResponseModel model)
        {
            var likerResponse = _mapper.Map<LikerResponse>(model);
            await _uow.LikerResponseRepository.AddAsync(likerResponse);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task LikeTopicAsync(LikerTopicModel model)
        {
            var likerTopic = _mapper.Map<LikerTopic>(model);
            await _uow.LikerTopicRepository.AddAsync(likerTopic);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task RemoveLikeResponseAsync(LikerResponseModel model)
        {
            var likerResponse = _mapper.Map<LikerResponse>(model);
            await Task.Run(() => _uow.LikerResponseRepository.Delete(likerResponse));
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task RemoveLikeTopicAsync(LikerTopicModel model)
        {
            var likerTopic = _mapper.Map<LikerTopic>(model);
            await Task.Run(() => _uow.LikerTopicRepository.Delete(likerTopic));
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task UpdateAsync(UserModel model)
        {
            if (_validator.Validate(model).Errors.Count != 0)
            {
                throw new ForumException("The user has incorrect data!");
            }

            var user = _mapper.Map<User>(model);

            await _uow.UserRepository.Update(user);
            await _uow.SaveAsync();
        }
    }
}
