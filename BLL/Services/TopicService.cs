﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using FluentValidation;

namespace BLL.Services
{
    /// <inheritdoc />
    public class TopicService : ITopicService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<TopicModel> _validator;

        /// <summary>
        /// Topic Service Constructor
        /// </summary>
        /// <param name="uow">Unit of work</param>
        /// <param name="mapper">Custom Mapper</param>
        /// <param name="validator">Custom Validator for Model</param>
        public TopicService(IUnitOfWork uow, IMapper mapper, AbstractValidator<TopicModel> validator)
        {
            _uow = uow;
            _mapper = mapper;
            _validator = validator;
        }

        /// <inheritdoc />
        public async Task AddAsync(TopicModel model)
        {
            if(_validator.Validate(model).Errors.Count != 0)
            {
                throw new ForumException("The topic has incorrect data!");
            }

            var topic = _mapper.Map<Topic>(model);

            await _uow.TopicRepository.AddAsync(topic);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task AddTagAsync(TopicTagModel model)
        {
            var topicTag = _mapper.Map<TopicTag>(model);

            await _uow.TopicTagRepository.AddAsync(topicTag);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task ComplainAboutTopicAsync(int topicId)
        {
            var topic = await _uow.TopicRepository.GetByIdWithDetailsAsync(topicId);
            topic.Complaints++;

            await _uow.TopicRepository.Update(topic);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task DeleteAsync(int modelId)
        {
            await _uow.TopicRepository.DeleteByIdAsync(modelId);
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TopicModel>> GetAllAsync()
        {
            var topics = await _uow.TopicRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<TopicModel>>(topics);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TopicModel>> GetByFilterAsync(FilterModel filter)
        {
            var topics = await _uow.TopicRepository.GetAllWithDetailsAsync();
            var models = _mapper.Map<IEnumerable<TopicModel>>(topics);
            List<TopicModel> taggedTopics = new List<TopicModel>();

            if (filter != null)
            {
                if (filter.PublicationDate.HasValue)
                {
                    models = models.Where(t => t.PublicationDate == filter.PublicationDate);
                }

                if (filter.TagIds != null)
                {
                    foreach (var item in models)
                    {
                        foreach (var prop in item.TopicTagIds)
                        {
                            if (filter.TagIds.Contains(prop.TagId))
                            {
                                taggedTopics.Add(item);
                            }
                        }
                    }
                }
            }

            return taggedTopics.Distinct();
        }

        /// <inheritdoc />
        public async Task<TopicModel> GetByIdAsync(int id)
        {
            var topic = await _uow.TopicRepository.GetByIdWithDetailsAsync(id);
            return _mapper.Map<TopicModel>(topic);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TopicModel>> GetByPageModelAsync(PageModel pageModel)
        {
            var topics = await _uow.TopicRepository.GetAllWithDetailsAsync();
            topics = topics.Skip((pageModel.PageNumber - 1) * pageModel.PageSize)
            .Take(pageModel.PageSize);
            return _mapper.Map<IEnumerable<TopicModel>>(topics);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TagModel>> GetTagsAsync()
        {
            var tags = await _uow.TagRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<TagModel>>(tags);
        }

        /// <inheritdoc />

        public async Task RemoveTagAsync(TopicTagModel model)
        {
            var topicTag = _mapper.Map<TopicTag>(model);

            await Task.Run(() => _uow.TopicTagRepository.Delete(topicTag));
            await _uow.SaveAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TopicModel>> SortByLikesAsync()
        {
            var topics = await _uow.TopicRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<TopicModel>>(topics.OrderByDescending(t => t.LikedBy.Count));
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TopicModel>> SortByPublicationDateAsync()
        {
            var topics = await _uow.TopicRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<TopicModel>>(topics.OrderByDescending(t => t.PublicationDate));
        }

        /// <inheritdoc />
        public async Task UpdateAsync(TopicModel model)
        {
            if (_validator.Validate(model).Errors.Count != 0)
            {
                throw new ForumException("The topic has incorrect data!");
            }

            var topic = _mapper.Map<Topic>(model);

            await _uow.TopicRepository.Update(topic);
            await _uow.SaveAsync();
        }
    }
}
