﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using BLL.Models;
using DAL.Entities;

namespace BLL.Mapper
{
    /// <summary>
    /// Custom Mapper for relatioship between light models and EF Core entites
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        /// <summary>
        /// Defautl AutoMapperProfile Constructor
        /// </summary>
        public AutoMapperProfile()
        {
            CreateMap<Topic, TopicModel>()
               .ForMember(tm => tm.ResponsesIds, t => t.MapFrom(x => x.Responses.Select(r => r.Id)))
               .ForMember(tm => tm.TopicTagIds, t => t.MapFrom(x => x.TopicTags.Select(tt =>
                    new TopicTagModel { TagId = tt.TagId, TopicId = tt.TopicId })))
               .ForMember(rm => rm.LikedByIds, r => r.MapFrom(x => x.LikedBy.Select(lt =>
                    new LikerTopicModel { UserId = lt.UserId, TopicId = lt.TopicId })))
               .ReverseMap();

            CreateMap<Response, ResponseModel>()
               .ForMember(rm => rm.CommentIds, r => r.MapFrom(x => x.Comments.Select(c => c.Id)))
               .ForMember(rm => rm.LikedByIds, r => r.MapFrom(x => x.LikedBy.Select(lr =>
                    new LikerResponseModel { UserId = lr.UserId, ResponseId = lr.ResponseId })))
               .ReverseMap();

            CreateMap<User, UserModel>()
               .ForMember(um => um.CreatedTopicIds, u => u.MapFrom(x => x.CreatedTopics.Select(c => c.Id)))
               .ForMember(um => um.CreatedResponseIds, u => u.MapFrom(x => x.CreatedResponses.Select(c => c.Id)))
               .ForMember(um => um.CreatedCommentIds, u => u.MapFrom(x => x.CreatedComments.Select(c => c.Id)))
               .ForMember(um => um.LikedResponseIds, u => u.MapFrom(x => x.LikedResponses.Select(lr =>
                    new LikerResponseModel { UserId = lr.UserId, ResponseId = lr.ResponseId })))
               .ForMember(um => um.LikedTopicIds, u => u.MapFrom(x => x.LikedTopics.Select(lt => 
                    new LikerTopicModel { UserId = lt.UserId, TopicId = lt.TopicId })))
               .ForMember(um => um.Email, u => u.MapFrom(x => x.UserCredentials.Email))
               .ForMember(um => um.Nickname, u => u.MapFrom(x => x.UserCredentials.UserName))
               .ForMember(um => um.UserCredentialsId, u => u.MapFrom(x => x.UserCredentials.Id))
               .ReverseMap();

            CreateMap<Comment, CommentModel>()
                .ReverseMap();

            CreateMap<Tag, TagModel>()
               .ForMember(tm => tm.TopicTagIds, t => t.MapFrom(x => x.TopicTags.Select(tt => 
                    new TopicTagModel { TagId = tt.TagId, TopicId = tt.TopicId })))
               .ReverseMap();

            CreateMap<TopicTag, TopicTagModel>()
                .ReverseMap();

            CreateMap<LikerResponse, LikerResponseModel>()
                .ReverseMap();

            CreateMap<LikerTopic, LikerTopicModel>()
                .ReverseMap();
        }
    }
}
