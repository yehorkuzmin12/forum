﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;

namespace BLL.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
        public DateTime BirthDate { get; set; }
        public string UserCredentialsId { get; set; }
        public virtual ICollection<int> CreatedTopicIds { get; set; }
        public virtual ICollection<int> CreatedResponseIds { get; set; }
        public virtual ICollection<LikerTopicModel> LikedTopicIds { get; set; }
        public virtual ICollection<LikerResponseModel> LikedResponseIds { get; set; }
        public virtual ICollection<int> CreatedCommentIds { get; set; }
    }
}
