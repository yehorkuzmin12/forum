﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class PageModel
    {
        public int PageSize { get; set; } = 1;
        public int PageNumber { get; set; } = 1;
    }
}
